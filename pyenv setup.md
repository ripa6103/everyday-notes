### Setup on MAC OS

- Run this command download the pyenv virtual environment
```
curl -L https://github.com/pyenv/pyenv-installer/raw/master/bin/pyenv-installer | bash   
```
- use the touch command to create a environment file name zshenv. Use '.' before file name to make the file hidden. Create this file under Users/username folder

```
touch .zshenv
```

- Copy and paste the following lines to add pyenv as the zsh command

```
export PYENV_ROOT="$HOME"/.pyenv
export PATH="$PYENV_ROOT"/bin:"$PATH"
if command -v pyenv 1>/dev/null 2>&1; then
  eval "$(pyenv init -)"
fi
eval "$(pyenv virtualenv-init -)"
```

- now write the following command on the command prompt to check if the command is actually added to the environment commands
```
pyenv init
```

### Setup on Raspberry pi

- Check the os version
```
cat /etc/debian_version
```
in my case it is 11.2.
- the home directory for raspi is /home/pi/
- got to /tmp/ directory with ```cd /tmp/```
- Clone the pyenv repo here
```
git clone  https://github.com/pyenv/pyenv.git ~/.pyenv
```
- ls -al would show you the .pyenv file in the root directory
- Now stay in the Home directory and run following commands

```
echo 'export PYENV_ROOT="${HOME}/.pyenv"' >> ~/.bashrc
echo 'if [ -d "${PYENV_ROOT}" ]; then' >> ~/.bashrc
echo '    export PATH=${PYENV_ROOT}/bin:$PATH' >> ~/.bashrc
echo '    eval "$(pyenv init -)"' >> ~/.bashrc
echo 'fi' >> ~/.bashrc
exec $SHELL -l
```
[This resource](http://fabacademy.org/2020/labs/kannai/students/tatsuro-homma/project/RaspPi_P_01_setupPyenv.html) was helpful in installing the pyenv on raspi

### Using Pyenv

Once pyenv is installed you can move to the project directory

```
pyenv install 3.10.2
pyenv intsall -l # gives list of available python and anconda versions
```
Now set the local python environment

```
pyenv local 3.10.2
```
The above doesnot seem to work. Added the following to .zshrc. I created it as I did  not have one already created by default

```
eval "$(pyenv init --path)"
eval "$(pyenv init -)"
```

- Added same lines to .bashrc on Raspberry Pi and then the thing functions. For raspi reboot was required
- Check by using ```python or which python``` command if the thing works fine
- run the following command to create python local python image

```
python -m venv . or python3 -m venv .
```
- activate the python environment int the nxt step

```source bin/activate```

- unset the current local pyenv
```
pyenv local --unset
```
