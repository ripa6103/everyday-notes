- type the following command to go inside the raspi config
```
sudo raspi-config
```
- The config window opens --> Navigate to Interfacing options --> I2C --> Select Yes --> hit ok --> and finish
- Depending upon raspi you might need to reboot it for the change to be recognized
- To check it got back to the command line and write the following command
```
ls /dev/*i2c*
```
- If you get some /dev/i2c-x type results...hurray you have enalbled your i2c ports
- The following command shows you the i2c devices connected to your raspi and its address

```
sudo i2cdetect -y 1
```